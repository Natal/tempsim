import pygame
import meshcreator
import roomcreator
import sys
from pygame.locals import *
from box import Box
from mesh import Mesh
from room import Room

class DrawingOptions:
   def __init__(self, g, b):
     self.grid = g
     self.bilinear = b

def draw_room_shape(screen, room):
 pixarr = pygame.surfarray.pixels3d(screen)
 for i in range(0, room.width):
   for j in range(0, room.height):
     p = [i, j]
     if not room.intersects(p):
       pixarr[i, j] = [0, 0, 0]
     else:
       pixarr[i, j] = [100, 100, 100]

def draw_background(screen, room):
 pixarr = pygame.surfarray.pixels3d(screen)
 for i in range(0, room.width):
   for j in range(0, room.height):
     p = [i, j]
     if not room.intersects(p):
       pixarr[i, j] = [0, 0, 0]

def draw_mesh(room, screen, m, drawing_options):
  draw_room_shape(screen.image, room)
  m.colorise()
  if drawing_options.bilinear:
    m.bilinearcolor(pygame.surfarray.pixels3d(screen.image))
  m.draw(screen.image)
  draw_background(screen.image, room)
  if drawing_options.grid:
    m.draw_grid(screen.image)
#  pygame.display.update()


def process_mesh(room, m):
  m.assign_room(room)
  m.solve(float(sys.argv[1]), float(sys.argv[2]))

def precompute_screens(room, screen, m):
   print 'Precomputing screens (may take a while)...'

   screen1 = Box([0,0,0], [room.width, room.height], [0,0])
   draw_mesh(room, screen1, m, DrawingOptions(False, False))
   screen2 = Box([0,0,0], [room.width, room.height], [0,0])
   draw_mesh(room, screen2, m, DrawingOptions(False, True))
   screen3 = Box([0,0,0], [room.width, room.height], [0,0])
   draw_mesh(room, screen3, m, DrawingOptions(True, False))
   screen4 = Box([0,0,0], [room.width, room.height], [0,0])
   draw_mesh(room, screen4, m, DrawingOptions(True, True))
   print 'Precomputing done'
   return [screen1, screen2, screen3, screen4]

def draw_screen(screens, screen, do):
   i = 0
   for g in [False, True]:
     for b in [False, True]:
       if (do.grid == g and do.bilinear == b):
         screen.blit(screens[i].image, screens[i].rect)
         pygame.display.update()
         return
       i = i + 1

if (len(sys.argv) < 1):
  print 'Usage:./tempsym [conductivity] [f]'

screen_x = 400
screen_y = 400
white = [255, 255, 255]
red = [255, 0, 0]
size = 10


pygame.init()
screen = pygame.display.set_mode([screen_x, screen_y])

roomcreator.draw_room(screen)

room = Room("room.bmp")

screen_x = room.width
screen_y = room.height


delta = [0, 0]

draw_room_shape(screen, room)
grid = meshcreator.open_creator(screen,
                                pygame.surfarray.pixels2d(screen).copy(),
                                screen_x,
                                screen_y)

m = Mesh(screen_x, screen_y, grid, delta)
do = DrawingOptions(True, False)
process_mesh(room, m)

m.to_file()

screens = precompute_screens(room, screen, m)
draw_screen(screens, screen, do)

run = True

while run:
   event = pygame.event.poll()
   if event.type == KEYDOWN:
      if event.key == K_ESCAPE:
        run = False
      else :
        if event.key == 103:
          do.grid = not do.grid
          draw_screen(screens, screen, do)
        if event.key == 98:
          do.bilinear = not do.bilinear
          draw_screen(screens, screen, do)
   else:
     if event.type == QUIT:
      run = False
