from PIL import Image

class Room:
  def __init__(self, img_path):
    img = Image.open(img_path)
    self.data_ = img.load()
    (self.height, self.width) = img.size

  def intersects(self, point):
    return self.data_[point[0], point[1]] == (0, 0, 0)

  def intersects_list(self, points):
        for p in points:
          if self.intersects(p):
             return True
        return False

