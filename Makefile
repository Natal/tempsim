all:
	python main.py 10 1

plot: res.data
	gnuplot -p -e "splot 'res.data' with points palett"

clean:
	rm -rf .*.swp
	rm -rf *.pyc
