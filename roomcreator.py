import pygame
from pygame.locals import *

def print_poly(screen, point_list):
  screen.fill([255,255,255])
  if (len(point_list) > 2):
    pygame.draw.polygon(screen, [0,0,0], point_list)
    pygame.display.update()
  else:
    if (len(point_list) == 2):
      pygame.draw.line(screen, [0,0,0], point_list[0], point_list[1])
      pygame.display.update()

def draw_room(screen):
  point_list = []
  run = True
  screen.fill([255,255,255])
  pygame.display.update()
  while run:
   event = pygame.event.poll()
   if event.type == KEYDOWN:
      if event.key == K_ESCAPE:
        run = False
      if event.key == K_u:
        if (len(point_list) > 0):
          point_list.pop(len(point_list) - 1)
          print_poly(screen, point_list)
   if event.type == pygame.MOUSEBUTTONDOWN:
     if event.button == 1:
       point_list.append(event.pos)
       print_poly(screen, point_list)

   pygame.image.save(screen, "room.bmp")

