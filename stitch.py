import pygame
from pygame.locals import *
from numpy import *
from box import Box
from vector import *

def adjust(val):
  if val == 0: return 1
  else: return val

class Stitch:

  pos_ = [0., 0.]

  def __init__(self, pos, up, left):

    white = [255, 255, 255]

    self.pos_ = pos

    self.neighbors_ = [up, left]

    self.dist_ = [1, 1, 1, 1]

    self.lines_ = []

    self.color_ = [0, 0, 0]


    self.in_room_ = False

    self.index_ = -1


    i = 0
    for s in self.neighbors_:
      if s is not None:
        dist = s.pos() - self.pos_
        self.dist_[i] = abs(dist)
        dist = complex(adjust(dist.real), adjust(dist.imag))
        self.lines_.append(Box(white, [dist.real, dist.imag],
                                      [pos.real, pos.imag]))
      i = i + 1

    rect = [self.dist_[1] + self.dist_[3], self.dist_[0] + self.dist_[2]]
    rect = [2, 2]
    pos = self.couple_pos()
    pos[0] -= self.dist_[1]
    pos[1] -= self.dist_[0]
    self.spot_ = Box(white, rect, pos)

  def neighbors(self):
    return self.neighbors_

  def pos(self):
    return self.pos_

  def index(self):
    return self.index_

  def vertices(self):
    pos = [self.couple_pos()]
    if self.neighbors_[0] is not None:
      pos.append(self.neighbors_[0].couple_pos())
      if self.neighbors_[0].neighbors_[1] is not None:
        pos.append(self.neighbors_[0].neighbors_[1].couple_pos())
    if self.neighbors_[1] is not None:
      pos.append(self.neighbors_[1].couple_pos())
    return pos

  def index_set(self, idx):
    self.index_ = idx

  def set_in_room(self):
    self.change_col([0,0,255])
    self.in_room_ = True

  def initialise(self):
    for n in self.neighbors_:
      if (n is not None and not n.in_room_):
        self.change_col([0, 255, 0])
        return

  def draw(self, screen):
   # for b in self.lines_ :
   #   screen.blit(b.image, b.rect)
    screen.blit(self.spot_.image, self.spot_.rect)

  def draw_lines(self, screen):
    for b in self.lines_ :
      screen.blit(b.image, b.rect)

  def assign_rd(self, down, right):
    self.neighbors_.append(down)
    self.neighbors_.append(right)
    if down is not None:
      self.dist_[2] = abs(self.pos() - down.pos())
    if right is not None:
      self.dist_[3] = abs(self.pos() - right.pos())

    #if down is not None:
    #  self.lines_.extend(down.getlu_lines())
    #if right is not None:
    #  self.lines_.extend(right.getlu_lines())

  def getlu_lines(self):
    return [self.lines_[0], self.lines_[1]]

  def couple_pos(self):
     return [(int)(round(self.pos_.real)), (int)(round(self.pos_.imag))]

  def center_pos(self):
     pos = self.couple_pos()
     pos[0] -= self.dist_[1] / 2
     pos[1] -= self.dist_[0] / 2
     return pos

  def change_col(self, color):
     self.color_ = color
     self.spot_.change_col(color)
    # for l in self.lines_:
    #    l.change_col(color)

  #uldr : up left down right
  def get_direct_neighbor_index(self, uldr):
    n = self.neighbors_[uldr]
    if n is not None:
      return n.index()
    return -1

  def get_2nd_neighbor_index(self, uldr1, uldr2):
    n = self.neighbors_[uldr1]
    if n is not None:
      return n.get_direct_neighbor_index(uldr2)
    return -1

  def get_neighbor_dist(self, uldr1, uldr2):
    n = self.neighbors_[uldr1]
    if n is not None:
      return n.dist_[uldr2]
    print 'Warning: cannot access required stitch'
    return 1

  def posorzero(val):
    if val < 0:
      return 0
    return 1

  def compute_column(self, size_col, mat, offset, conductivity):

# first part of the equation (dealing with dx)

    el = self.dist_[1]
    er = self.dist_[3]
    e2r = self.get_neighbor_dist(3, 3)

    idl = self.get_direct_neighbor_index(1)
    idr = self.get_direct_neighbor_index(3)
    id2r = self.get_2nd_neighbor_index(3, 3)

    c1x = 1 / ((el + er) * e2r)
    c2x = 1 / ((el + er) * el)

    if idl > 0:
      mat[offset + idl] += -c2x * conductivity
    if idr > 0:
      mat[offset + idr] += c1x  * conductivity
    if id2r > 0:
      mat[offset + id2r] += -c1x * conductivity
    mat[offset + self.index_] += c2x * conductivity


# second part of the equation (dealing with dy)

    eu = self.dist_[0]
    ed = self.dist_[2]
    e2d = self.get_neighbor_dist(2, 2)

    idu = self.get_direct_neighbor_index(0)
    idd = self.get_direct_neighbor_index(2)
    id2d = self.get_2nd_neighbor_index(2, 2)

    c1y = 1 / ((eu + ed) * e2d)
    c2y = 1 / ((eu + ed) * eu)

    if idu > 0:
      mat[offset + idu] += -c2y * conductivity
    if idd > 0:
      mat[offset + idd] += c1y  * conductivity
    if id2d > 0:
      mat[offset + id2d] += -c1y * conductivity
    mat[offset + self.index_] += c2y * conductivity
    return mat

  def color_lu(self):
    if self.color_ != [255, 0, 0]:
     self.color_ = [255, 0, 0]
     self.spot_.change_col([255, 0, 0])
     for l in self.lines_:
        l.change_col([255, 0 ,0])
     if self.neighbors_[0] is not None:
       self.neighbors_[0].color_lu()
     if self.neighbors_[1] is not None:
       self.neighbors_[1].color_lu()

  def color_rd(self):
    if self.color_ != [255, 0, 0]:
     self.color_ = [255, 0, 0]
     self.spot_.change_col([255, 0, 0])
     for l in self.lines_:
        l.change_col([255, 0 ,0])
     if self.neighbors_[2] is not None:
       self.neighbors_[2].color_rd()
     if self.neighbors_[3] is not None:
       self.neighbors_[3].color_rd()

  def vect_pos(self):
    return Vector(self.pos_.real, self.pos_.imag)

  def upper_left_exits(self):
    n = self.neighbors_
    res = (n[0] is not None) and (n[1] is not None)
    res = res and (n[0].neighbors_[1] is not None)
    return res


  def bilinearcoloring(self, pixarr):

    if not self.upper_left_exits():
      return

    c1 = self.color_
    c2 = self.neighbors_[0].color_
    c3 = self.neighbors_[1].color_
    c4 = self.neighbors_[0].neighbors_[1].color_

    p1 = self.vect_pos()
    p2 = self.neighbors_[0].vect_pos()
    p3 = self.neighbors_[1].vect_pos()
    p4 = self.neighbors_[0].neighbors_[1].vect_pos()

    xsize = int(round(self.dist_[1]))
    ysize = int(round(self.dist_[0]))

    diag = Distance(p1, p4)

    for x in range(0, xsize + 1):
      for y in range(0, ysize + 1):
        pos = Vector(p1.x - x, p1.y - y)
        co1 = 1 - Distance(p1, pos) / diag
        co2 = 1 - Distance(p2, pos) / diag
        co3 = 1 - Distance(p3, pos) / diag
        co4 = 1 - Distance(p4, pos) / diag
        l = [col_mult(c1, co1), col_mult(c2, co2), col_mult(c3, co3), col_mult(c4, co4)]
        pixarr[pos.x, pos.y] = col_sum(l)
