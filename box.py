import pygame

class Box(pygame.sprite.Sprite):
    def __init__(self, color, size, initial_position):

      if size[0] < 0:
        size[0] = -size[0]
        initial_position[0] -= size[0]
      if size[1] < 0:
        size[1] = -size[1]
        initial_position[1] -= size[1]


      # All sprite classes should extend pygame.sprite.Sprite. This
      # gives you several important internal methods that you probably
      # don't need or want to write yourself. Even if you do rewrite
      # the internal methods, you should extend Sprite, so things like
      # isinstance(obj, pygame.sprite.Sprite) return true on it.
      pygame.sprite.Sprite.__init__(self)

      # Create the image that will be displayed and fill it with the
      # right color.
      self.image = pygame.Surface(size)
      self.color_ = color
      self.image.fill(color)

      # Make our top-left corner the passed-in location.
      self.rect = self.image.get_rect()
      self.rect.topleft = initial_position

    def change_col(self, color):
      self.color_ = color
      self.image.fill(color)

    def get_col(self):
      return self.color_
