import pygame
from pygame.locals import *

class Grid:
  xarr_ = []
  yarr_ = []
  def __init__(self, size_x, size_y, screen_w, screen_h):
    self.xarr_ = range(0, screen_w, size_x)
    self.yarr_ = range(0, screen_h, size_y)
    self.screen_w_ = screen_w
    self.screen_h_ = screen_h

  def add_x(self, pos_x):
    i = 0
    for x in self.xarr_:
      if x > pos_x:
        x_val = (self.xarr_[i] + self.xarr_[i - 1]) / 2
        if x_val == x:
          return
        self.xarr_.insert(i, x_val)
        return
      i = i + 1
  def add_y(self, pos_y):
    i = 0
    for y in self.yarr_:
      if y > pos_y:
        y_val = (self.yarr_[i] + self.yarr_[i - 1]) / 2
        if y_val == y:
          return
        self.yarr_.insert(i, (self.yarr_[i] + self.yarr_[i - 1]) / 2)
        return
      i = i + 1

  def draw(self, screen):
    white = [255, 255, 255]
    for x in self.xarr_:
      pygame.draw.line(screen, white, [x, 0], [x, self.screen_h_])
    for y in self.yarr_:
      pygame.draw.line(screen, white, [0, y], [self.screen_w_, y])
    pygame.display.update()


def open_creator(screen, nogrid, width, height):
  run = True
  size_x = 10
  size_y = 10
  print 'stitch size: {0}'.format(size_x)
  grid = Grid(size_x, size_y, width, height)
  grid.draw(screen)
  while run:
   event = pygame.event.poll()
   if event.type == KEYDOWN:
      if event.key == K_ESCAPE:
        run = False
   if event.type == pygame.MOUSEBUTTONDOWN:
     if event.button == 4:
       size_x += 1
       size_y += 1
       print 'stitch size: {0}'.format(size_x)
       pygame.surfarray.blit_array(screen, nogrid)
       grid = Grid(size_x, size_y, width, height)
       grid.draw(screen)
     else:
       if event.button == 5:
         if size_x > 1:
           size_x -= 1
         if size_y > 1:
           size_y -= 1
         print 'stitch size: {0}'.format(size_x)
         pygame.surfarray.blit_array(screen, nogrid)
         grid = Grid(size_x, size_y, width, height)
         grid.draw(screen)
       else:
         if event.button == 1:
           grid.add_x(event.pos[0])
           grid.draw(screen)
         if event.button == 3:
           grid.add_y(event.pos[1])
           grid.draw(screen)
   else:
     if event.type == QUIT:
      run = False
  return grid
