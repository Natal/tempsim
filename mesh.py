import pygame
from pygame.locals import *
from numpy import *
from box import Box
from stitch import Stitch
from room import Room
from meshcreator import Grid
from numpy.linalg import solve

class Mesh:


  all_stitches_ = []
  stitches_ = []


  def __init__(self, sx, sy, grid, delta):

    up = None
    left = None

    self.solutions_ = None

    xsize = len(grid.xarr_)
    ysize = len(grid.yarr_)

    i = 0
    for x in grid.xarr_:
      j = 0
      self.all_stitches_.append([])
      up = None
      for y in grid.yarr_:
        if i > 0 : left = self.all_stitches_[i - 1][j]
        else : left = None
        cur = Stitch(complex(x, y), up, left)
        self.all_stitches_[i].append(cur)
        up = cur
        j = j + 1
      i = i +1


    right = None
    down = None

    for x in range(xsize - 1, -1, -1):
      down = None
      for y in range(ysize - 1, -1, -1):
        if x < xsize - 1 : right = self.all_stitches_[x + 1][y]
        else : right = None
        self.all_stitches_[x][y].assign_rd(down, right)
        down = self.all_stitches_[x][y]

  def draw_grid(self, screen):
    for b in self.all_stitches_:
      for bx in b :
        bx.draw_lines(screen)

  def draw(self, screen):
    for bx in self.stitches_:
        bx.draw(screen)

  def to_file(self):
    i = 0
    f = open('res.data', 'w+')
    for t, s in map(None, self.solutions_, self.stitches_):
      f.write('{0} {1} {2}\n'.format(s.pos_.real, s.pos_.imag, t))
    f.close()

  def color_it(self):
    self.all_stitches_[self.size_x - 1][self.size_y - 1].color_lu()

 # assign_room finds which stitches are in the room and gives each
 # one of them an index. Which will match the index in the final
 # matrix

  def assign_room(self, room):
    index = 0
    for asx in self.all_stitches_:
      for s in asx:
        if (room.intersects(s.center_pos()) or
            room.intersects_list(s.vertices())):
          s.set_in_room()
          s.index_set(index)
          self.stitches_.append(s)
          index = index + 1

    for s in self.stitches_:
      s.initialise()

  def solve(self, conductivity, fxy_val):
    size_col = len(self.stitches_)
    matrix_arr = zeros(size_col * size_col)
    print 'System size : {0} points (equations)'.format(size_col)
    print 'Building system...'
    fxy = ones(size_col)
    fxy.fill(fxy_val)
    offset = 0
    for s in self.stitches_:
      s.compute_column(size_col, matrix_arr, offset, conductivity)
      offset += size_col

    mat = matrix_arr.reshape(size_col, size_col)
    print 'Solving system...'
    self.solutions_ = solve(mat, fxy)
    print 'System Solved'

  def colorise(self):
    i = 0
    values = self.solutions_
    maxval = values.max()
    minval = values.min()
    sumval = (abs(minval) + abs(maxval))
    for s in self.stitches_:
      col = int(round(255. * (values[i] + abs(minval)) / sumval))
      s.change_col([col, 0, 255 - col])
      i = i + 1

  def bilinearcolor(self, pixels3d):
    for s in self.stitches_:
     s.bilinearcoloring(pixels3d)


